
import './App.css';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import order from './order.jpg'


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop:'6px'
  },
  active: {
    '& $line': {
      borderColor: 'blue',
    },
  },
  completed: {
    '& $line': {
      borderColor: 'green',
    },
  },
  line: {
    borderColor: '#eaeaf0',
    borderTopWidth: 3,
    borderRadius: 1,
  },
  
}));

function App() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);

  
  
  const steps = getSteps();
  function getSteps() {
    return ['Order Confirmed', 'Order Packed', 'Order Shipped', 'Order Assign to Delivery Boy', ''];
  }
  
  return (
    <div className="app">
        <div className="top">
            
            <div className="top__left">
              <div className="image">
                <img src={order}/>
                <div className="payment">
                <h2>₹ 33,990</h2> 
                <h2 style={{borderLeft:`2px solid rgba(47, 47, 47,0.5)`}}>Paid</h2>
                </div>
               
              </div>
              <div className="detail">
                  <div className="info">
                      <div className="Name">                         
                          <h2>Asus VivoBook E12 Celerom Duel Core-(2GB/32GB EMMC Storage</h2>
                          <h2>Windows 10 Home)E203MA-FD014T Thin and Light Laptop (11.6 inch,Star</h2>
                      </div>
                      <div className="size">
                            <h2>Product Quantity: <strong>1</strong></h2>
                            <h2>Delivery Boy:<strong>Ankush Sharma</strong></h2>
                      </div>
                  </div>
                  <div className="product__status">
                    <h2> Product Status</h2>
                    <div className={classes.root}>
                    <Stepper activeStep={activeStep} alternativeLabel>
                      {steps.map((label) => (
                          <Step key={label}>
                          <StepLabel>{label}</StepLabel>
                      </Step>
                      ))}
                    </Stepper>
                    
                  </div>
                  </div>
              </div>
            </div>
            <div className="top__right">
              <div className="order__id">
                <p>Order ID: 1111111111 (1 Item)</p>
                <p>Placed on 12 Feb,2016</p>
              </div>
              <div className="order__address">
                  <h2>SHIPPING INFORMATION</h2>
                  <p>Contect person Name:Manoj Tiwari</p>
                  <p>Laxmi Nain,Behind Triumph Academy,shastri</p>
                  <p>Nagar , Laxmi Nain,Jaipur Rajastan-302001</p>
                  <p>Mobile No: +91 222 -(9999) -111</p>
              </div>
            </div>
      
        </div>
        <div className="bottom">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d121058.92836568356!2d73.79292696070254!3d18.524766326462743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c065144d8edf%3A0x3703b8095866c54b!2sShaniwar%20Wada!5e0!3m2!1sen!2sin!4v1611158194610!5m2!1sen!2sin"
         width="75%" height="500px" frameborder="0" 
          allowfullscreen
          >
        </iframe>
        <div className="order_map">
              <h4>Track Your Order On Map</h4> 
              <div className="order_vertical">
              <div className="stepper-wrapper-vertical">
                        <div className="horizontal__wrapper">
                          <div className="wrapper_top">
                          <div className="step__number"></div>
                          <div className="step__description">Order Confirmed</div>
                          <div className="divider__line"></div>
                          </div>                         
                        </div>
                        <div className="wrapper-vertical"style={{marginBottom:'15px', marginLeft:'50px'}} >
                        <div className="line"></div>
                            <div className="vertical__wrapper">
                                <div className="line"></div>
                                <div className="step_number"></div>
                                <div className="step__description">
                                   <p style={{fontSize:'12px'}}>8:30 AM-Shipping information received</p>
                                </div>
                                <div className="line"></div>
                            </div>
                        </div>
                        <div className="wrapper-vertical"style={{marginBottom:'15px', marginLeft:'50px'}} >
                            <div className="vertical__wrapper">
                                <div className="line"></div>
                                <div className="step_number"></div>
                                <div className="step__description">
                                <p style={{fontSize:'12px'}}>11:00 AM-Processed through UPS Facility</p>
                                </div>
                                <div className="line"></div>
                            </div>
                        </div>
                     

                        <div className="horizontal__wrapper">
                          <div className="step__number"></div>
                          <div className="step__description">Shipped to Airoplan</div>
                          <div className="divider__line"></div>
                      </div>
                      <div className="wrapper-vertical"style={{marginBottom:'15px', marginLeft:'50px'}} >
                        <div className="line"></div>
                            <div className="vertical__wrapper">
                                <div className="line"></div>
                                <div className="step_number"></div>
                                <div className="step__description">
                                <p style={{fontSize:'12px'}}>7:30 AM-Reached to Jaipur</p></div>
                                <div className="line"></div>
                            </div>
                        </div>
                        <div className="wrapper-vertical"style={{marginBottom:'15px', marginLeft:'50px'}} >
                            <div className="vertical__wrapper">
                                <div className="line"></div>
                                <div className="step_number"></div>
                                <div className="step__description"></div>
                                <p style={{fontSize:'12px'}}>11:00 AM-Checked Product packing </p>
                                <div className="line"></div>
                            </div>
                        </div>

                      <div className="horizontal__wrapper">
                        <div className="step__number"></div>
                        <div className="step__description">Dispatched for Delivery</div>
                        <div className="divider__line"></div>
                    </div>
                    <div className="wrapper-vertical"style={{marginBottom:'15px', marginLeft:'50px'}} >
                        <div className="line"></div>
                            <div className="vertical__wrapper">
                                <div className="line"></div>
                                <div className="step_number"></div>
                                <div className="step__description">
                                <p style={{fontSize:'12px'}}>10:30 AM-Assigned to Delivery Boy</p></div>
                                <div className="line"></div>
                            </div>
                        </div>
                        <div className="wrapper-vertical"style={{marginBottom:'15px', marginLeft:'50px'}} >
                            <div className="vertical__wrapper">
                                <div className="line"></div>
                                <div className="step_number"></div>
                                <div className="step__description">
                                <p style={{fontSize:'12px'}}>07:00 PM-Handover to Product Owner</p></div>
                                <div className="line"></div>
                            </div>
                        </div>
                    <div className="horizontal__wrapper">
                      <div className="step__number"></div>
          
                  </div>
              </div>
              </div>
          </div>
        </div>
    </div>
  );
}

export default App;
